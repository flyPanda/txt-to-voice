# txt-to-voice

#### 项目介绍
将txt文本自动转换为语音mp3格式的工具，带自动拆分章节功能。

#### 软件架构
c# winform程序开发 4.5framework


#### 安装教程

直接编译后，免安装，有framework就可以使用

#### 使用说明

1. 选择要合成音频的文本文件。
2. 选择音频文件保存位置
3. 点击预览（通过正则表达式拆分章节，懂正则表达式的小伙伴可自行编辑）
4. 点击转换

#### 效果截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1219/142208_69576a88_303583.png "无标题.png")
（转换中的效果）

![输入图片说明](https://images.gitee.com/uploads/images/2018/1219/142302_f6a4282d_303583.png "无标题.png")
（转换完成后保存的mp3文件）

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 个人博客
www.forever24.cn



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)